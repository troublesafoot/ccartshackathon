//
//  CCPlace.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/29/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class CCPlace: NSObject {
    
    var marker = GMSMarker()
    var image: UIImage? = nil
    var audioURL: URL? = nil
    var title: String = "Art Spot"
    var descriptionString: String = ""
    
    init(named title: String, location: CLLocationCoordinate2D?) {
        super.init()
        
        if let location = location {
            let lat = location.latitude
            let long = location.longitude
            marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        }
        marker.title = title
        
        self.title = title
        
    }

}

