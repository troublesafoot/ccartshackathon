//
//  ViewController.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/28/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        request()
        
    }

    func request() {
//        let urlPath = "https://httpbin.org/get"
        let urlPath = "https://api.myjson.com/bins/1dhl7h"
        Alamofire.request(urlPath).validate().responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                print(response.result.value ?? "no value")
            case .failure(let error):
                print(error)
            }
        }
    }

}
