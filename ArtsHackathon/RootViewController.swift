//
//  RootViewController.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/29/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit


class RootViewController: UIViewController {
    
    @IBOutlet weak var miniBarView: UIView!
    @IBOutlet weak var miniBarImageView: UIImageView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var miniBarButton: UIButton!
    @IBOutlet weak var miniBarTitleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "DidChangePlayerRateNotification"), object: nil, queue: nil) { (note: Notification) in
            
            let isPlaying = MediaManager.shared.player.rate > 0
            self.playPauseButton.isSelected = isPlaying
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "DidChangePlayerPlaceNotification"), object: nil, queue: nil) { (note: Notification) in
            guard let place = MediaManager.shared.currentPlace else { return }
            self.miniBarTitleLabel.text = place.title
        }
        
        miniBarView.layer.shadowOpacity = 0.7
        miniBarView.layer.shadowOffset = CGSize(width: 0, height: 1)
        miniBarView.layer.shadowRadius = 3.0
        miniBarView.layer.shadowColor = UIColor.darkGray.cgColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func miniBarAction(_ sender: Any) {
        guard let place = MediaManager.shared.currentPlace else { return }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MiniBarOpenCards"), object: place, userInfo: nil)
    }
    
    @IBAction func togglePlayPause(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        MediaManager.shared.togglePlayState()
    }
    
}
