//
//  MediaManager.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/29/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MediaManager: NSObject {
    
    static let shared = MediaManager()
    
    
    var player = AVPlayer()
    var currentPlace: CCPlace? = nil
    
    func prepare(_ place: CCPlace) {
        guard let url = place.audioURL else { return }
        
        currentPlace = place
        
        let asset = AVAsset(url: url)
        let playableKey = "playable"
        
        asset.loadValuesAsynchronously(forKeys: [playableKey]) {
            var error: NSError? = nil
            let status = asset.statusOfValue(forKey: playableKey, error: &error)
            switch status {
            case .loaded:
                break
            // Sucessfully loaded. Continue processing.
            case .failed: break
            // Handle error
            case .cancelled: break
            // Terminate processing
            default:
                print("playableKeys \(status)")
                break
                // Handle all other cases
            }
        }
        
        let playerItem = AVPlayerItem(asset: asset)
        
        player = AVPlayer(playerItem: playerItem)
        player.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidChangePlayerPlaceNotification"), object: player, userInfo: nil)
        
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard object is AVPlayer else { return }
        guard let keyPath = keyPath else { return }
        
        switch keyPath {
        case "status":
            print("status changed")
        case "rate":
            print("--rate--")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidChangePlayerRateNotification"), object: player, userInfo: nil)
        default:
            print("\(keyPath)")
        }
    }
    
    func togglePlayState() {
        
        if player.rate == 0.0 {
            player.play()
        } else {
            player.pause()
        }
    }

}
