//
//  GoogleMapViewController.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/28/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class GoogleMapViewController: UIViewController {
    
    let infoMarker = GMSMarker()

    override func viewDidLoad() {
        
        let camera = GMSCameraPosition.camera(withLatitude: 47.5571, longitude: -122.2845, zoom: 17.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        ApplicationManager.shared.addPlaces(to: mapView)
        
        mapView.delegate = self
        
        mapView.accessibilityElementsHidden = true
        mapView.isMyLocationEnabled = true
        
        
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        // Open Card view segue when root mini bar is pressed
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "MiniBarOpenCards"), object: nil, queue: nil) { (note: Notification) in
            let sender = MediaManager.shared.currentPlace
            self.performSegue(withIdentifier: "CardSegueIdentifer", sender: sender)
        }
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "ShowScrollMapToPositionNotification"), object: nil, queue: nil) { (note: Notification) in
            if let position = note.userInfo?["newPosition"] as? CLLocationCoordinate2D, position.latitude != -180 {
                
                mapView.animate(toLocation: position)
            }
            
            
        
        }
        
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let cardViewController = segue.destination as? CardsViewController,
            let place = sender as? CCPlace,
            segue.identifier == "CardSegueIdentifer" {
                cardViewController.presentingPlace = place
            
        }
    }
 

}


extension GoogleMapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let places = ApplicationManager.shared.places
        
        let place = places.filter({ $0.marker == marker}).first
        
        performSegue(withIdentifier: "CardSegueIdentifer", sender: place)
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String,
                 name: String, location: CLLocationCoordinate2D) {
        infoMarker.position = location
        infoMarker.title = name
        infoMarker.opacity = 0
        infoMarker.infoWindowAnchor.y = 1
        infoMarker.map = mapView
        mapView.selectedMarker = infoMarker
    }
    
}

