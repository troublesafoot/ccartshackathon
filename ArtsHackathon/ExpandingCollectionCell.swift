//
//  ExpandingCollectionCell.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/28/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import expanding_collection

class ExpandingCollectionCell: BasePageCollectionCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    
        
        titleLabel.layer.shadowRadius = 3
        titleLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        titleLabel.layer.shadowOpacity = 0.2
        titleLabel.layer.shadowColor = UIColor.black.cgColor
    }

}
