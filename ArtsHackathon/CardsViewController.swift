//
//  CardsViewController.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/28/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import expanding_collection

class CardsViewController: ExpandingViewController {
    
    
    var openIndex: IndexPath? = nil
    var presentingPlace: CCPlace? = nil
    
    @IBAction func dismissAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        itemSize = CGSize(width: 256, height: 335)
        super.viewDidLoad()
        
        if let collectionView = collectionView {
            addGestureToView(collectionView)
            // register cell
            let nib = UINib(nibName: "ExpandingCollectionCell", bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let presentingPlace = presentingPlace {
            if let index = ApplicationManager.shared.places.index(of: presentingPlace) {
                let indexPath = IndexPath(item: index, section: 0)
                collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CardsViewController {
    
    
    func addGestureToView(_ toView: UIView) {
        
        let gesutereUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler(_:)))
        gesutereUp.direction = .up
        
        
        let gesutereDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler(_:)))
        gesutereDown.direction = .down
        
        toView.addGestureRecognizer(gesutereUp)
        toView.addGestureRecognizer(gesutereDown)
    }
    
    func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        let indexPath = IndexPath(row: currentIndex, section: 0)
        guard let cell  = collectionView?.cellForItem(at: indexPath) as? ExpandingCollectionCell else { return }
        // double swipe Up transition
        if cell.isOpened == true && sender.direction == .up {
//            pushToViewController(getViewController())
//            
//            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
//                rightButton.animationSelected(true)
//            }
        }
        
        let open = sender.direction == .up ? true : false
        cell.cellIsOpen(open)
        openIndex = indexPath
    }
    
}

extension CardsViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let places = ApplicationManager.shared.places
        guard currentIndex >= 0 else {
            return
        }
        guard currentIndex < places.count else {
            return
        }
        
        let currentPlace = places[currentIndex]
        let location = currentPlace.marker.position
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowScrollMapToPositionNotification"), object: self, userInfo: ["newPosition" : location])
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ApplicationManager.shared.places.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as? ExpandingCollectionCell else {
            return UICollectionViewCell()
        }
        
        let place = ApplicationManager.shared.place(for: indexPath)
        cell.titleLabel.text = place.title
        cell.backgroundImageView.image = place.image
        cell.descriptionLabel.text = place.descriptionString
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let place = ApplicationManager.shared.place(for: indexPath)
        MediaManager.shared.prepare(place)
        MediaManager.shared.player.play()
    }
    
}

