//
//  MapViewController.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/28/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapKit()
    }
    
}

extension MapViewController: MKMapViewDelegate {

    func setupMapKit() {

        mapView.delegate = self

        let source = MapItem(named: "Tuta Bella", at: CLLocationCoordinate2D(latitude: 47.5571, longitude: 122.2845))

        let destination = MapItem(named: "Library", at: CLLocationCoordinate2D(latitude: 47.5599, longitude: 122.2870))

        self.mapView.showAnnotations([source.annotation, destination.annotation], animated: true )

        let directionRequest = MKDirectionsRequest()
        directionRequest.source = source.mapItem
        directionRequest.destination = destination.mapItem
        directionRequest.transportType = .walking

        let directions = MKDirections(request: directionRequest)

        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }


    public func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {}


    public func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {}



    public func mapViewWillStartLoadingMap(_ mapView: MKMapView) {}


    public func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {}


    public func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {}



    public func mapViewWillStartRenderingMap(_ mapView: MKMapView) {}


    public func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {}


    // mapView:viewForAnnotation: provides the view for each annotation.
    // This method may be called for all or some of the added annotations.
    // For MapKit provided annotations (eg. MKUserLocation) return nil to use the MapKit provided annotation view.

//    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {}


    // mapView:didAddAnnotationViews: is called after the annotation views have been added and positioned in the map.
    // The delegate can implement this method to animate the adding of the annotations views.
    // Use the current positions of the annotation views as the destinations of the animation.

    public func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {}


    // mapView:annotationView:calloutAccessoryControlTapped: is called when the user taps on left & right callout accessory UIControls.

    public func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {}



    public func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {}


    public func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {}



    public func mapViewWillStartLocatingUser(_ mapView: MKMapView) {}


    public func mapViewDidStopLocatingUser(_ mapView: MKMapView) {}


    public func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {}


    public func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {}


    public func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {}


    public func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {}



    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .blue
        renderer.lineWidth = 4.0

        return renderer
    }


//    public func mapView(_ mapView: MKMapView, didAdd renderers: [MKOverlayRenderer]) {}

}


class MapItem: NSObject {

    var mapItem: MKMapItem!
    let annotation = MKPointAnnotation()

    init(named title: String, at coordinate: CLLocationCoordinate2D) {
        super.init()

        let sourcePlacemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        mapItem = MKMapItem(placemark: sourcePlacemark)

        annotation.title = title
        if let location = sourcePlacemark.location {
            annotation.coordinate = location.coordinate
        }
    }
}
