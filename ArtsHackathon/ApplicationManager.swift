//
//  ApplicationManager.swift
//  ArtsHackathon
//
//  Created by Mark Daneker on 4/29/17.
//  Copyright © 2017 tmd. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ApplicationManager: NSObject {
    
    static let shared = ApplicationManager()
    
    var places: [CCPlace]  = []
    
    func setupWithTestData() {
        
        let intro = CCPlace(named: "Introduction", location: nil)
        intro.image = UIImage()
        
        let introurlpath = Bundle.main.path(forResource: "intro", ofType: "mp3")
        let introurl = NSURL.fileURL(withPath: introurlpath!)
        intro.audioURL = introurl
    
        let gallery = CCPlace(named: "Columbia City Gallery", location: CLLocationCoordinate2D(latitude: 47.5582, longitude: -122.2849))
        gallery.image = #imageLiteral(resourceName: "gallery")
        gallery.descriptionString = "The Columbia City Gallery is an artists’ cooperative that brings together emerging and professional artists working in diverse media. The gallery provides exhibition space for local artists and guest artists from our culturally diverse community, as well as rental space for events, fundraisers, or parties. The Columbia City Gallery is a program of SEEDArts"
        
        let urlpath = Bundle.main.path(forResource: "gallery", ofType: "mp3")
        let url = NSURL.fileURL(withPath: urlpath!)
        gallery.audioURL = url
        
        let theater = CCPlace(named: "Columbia City Theater", location: CLLocationCoordinate2D(latitude: 47.5573, longitude: -122.2845))
        theater.image = #imageLiteral(resourceName: "theater_front")
        let turlpath = Bundle.main.path(forResource: "theater", ofType: "mp3")
        let turl = NSURL.fileURL(withPath: turlpath!)
        theater.audioURL = turl
        theater.descriptionString = "The theatre showed movies from the 1920’s to the 1950s. The theaters unusual T shape stems from a law requiring “amusement” businesses to be located more than 500 feet from a school. Thus the theatre had a marquee on Rainier Avenue, but its official entrance was at the end of a ramp beyond the retail shops along Rainier."
        
        let black = CCPlace(named: "Black and Tan", location: CLLocationCoordinate2D(latitude: 47.5515519, longitude: -122.2778416))
        black.image = #imageLiteral(resourceName: "blackandtan")
        let blackurlpath = Bundle.main.path(forResource: "black", ofType: "mp3")
        let blackurl = NSURL.fileURL(withPath: blackurlpath!)
        black.audioURL = blackurl
        black.descriptionString = "Inspired by Seattle’s Black & Tan Club prominent in the 1930's, the new Black & Tan Hall is a cooperatively-owned performing arts and restaurant venue which seeks to maintain Rainier Valley as a destination for cross-cultural arts and education events. Historically, Black & Tan clubs offered a haven for people of all races in an era when segregation dictated social boundaries. The B&T Hall embraces that inclusive ethos while celebrating Seattle’s rich music and arts history."
        
        places = [intro, gallery, theater, black]
    }
    
    
    
    func addPlaces(to mapView: GMSMapView) {
        for place in places {
            place.marker.map = mapView
        }
    }
    
    func place(for indexPath: IndexPath) -> CCPlace {
        let place = places[indexPath.row]
        
        return place
    }


}
